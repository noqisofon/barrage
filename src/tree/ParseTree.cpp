#include "Parser.hxx"

#include "tree/ParseTree.hxx"

namespace barrage {
    namespace tree {
        ParseTree::ParseTree() : parent( nullptr ) {
        }

        bool ParseTree::operator== ( const ParseTree &other ) const {
            return &other == this;
        }
    }
}
