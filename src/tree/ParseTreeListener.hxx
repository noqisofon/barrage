#pragma once

namespace barrage {
    namespace tree {
        class BARRAGE_CPP_PUBLIC ParseTreeListener {
        public:
            /*!
             *
             */
            virtual void enterEveryRule( Unique<barrage::ParserRuleContext> &context ) = 0;

            /*!
             *
             */
            virtual void enterTerminal( Unique<TerminalNode> &node ) = 0;

            /*!
             *
             */
            virtual void visitErrorNode( Unique<ErrorNode> &node ) = 0;

            /*!
             *
             */
            virtual void exitEveryRule( Unique<barrage::ParserRuleContext> &context ) = 0;
        };
    }
}