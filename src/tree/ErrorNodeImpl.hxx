#pragma once

#include "tree/ErrorNode.hxx"
#include "tree/TerminalNodeImpl.hxx"
#include "misc/Interval.hxx"

namespace barrage {
    namespace tree {
        class ErrorNodeImpl : public virtual TerminalNodeImpl, public virtual ErrorNode {
        public:
            ErrorNodeImpl( Unique<Token> &a_token );
            ~ErrorNodeImpl();
        };
    }
}