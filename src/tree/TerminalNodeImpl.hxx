#pragma once

#include "tree/TerminalNode.hxx"

namespace barrage {
    namespace tree {
        /*!
         *
         */
        class TerminalNodeImpl : public virtual TerminalNode {
        public:
            /*!
             *
             */
            TerminalNodeImpl( Shared<Token> a_symbol );

        public:
           /*!
            *
            */
            virtual Shared<Token> getSymbol() const;

            /*!
             *
             */
            virtual void setParent( Shared<RuleContext> &parent ) override;

        public:
            Shared<Token> symbol;
        };
    }
}