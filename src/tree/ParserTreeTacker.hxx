#pragma once

#include "barrage-common.hxx"
#include "tree/ParseTree.hxx"

namespace barrage {
    namespace tree {
        /*!
         *
         */
        class /* BARRAGE_CPP_PUBLIC */ ParserTreeTacker {
        public:
            template <typename _Type, typename ... _Args>
            Shared<_Type> createInstance( _Args && ...args ) {
                Shared<_Type>  result = std::make_shared( args... );

                allocated.push_back( result );

                return result;
            }

            void reset() {
                for ( auto entry : allocated ) {
                    entry.reset( nullptr );
                }
            }

        private:
            Vec< Shared<ParseTree> > allocated;
        };
    }
}
