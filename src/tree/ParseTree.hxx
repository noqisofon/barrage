#pragma once

#include "support/Any.hxx"
#include "barrage-common.hxx"

namespace barrage {
    namespace tree {
        /*!
         *
         */
        class BARRAGE_CPP_PUBLIC ParseTree {
        public:
            ParseTree();
            ParseTree( ParseTree const & ) = delete;

            virtual ~ParseTree() {}

        public:
            /*!
             *
             */
            virtual std::string toStringTree() = 0;
            /*!
             *
             */
            virtual std::string toStringTree( Unique<Parser> &parser ) = 0;

            /*!
             *
             */
            virtual std::string toString() = 0;

            /*!
             *
             */
            virtual bool operator== ( const ParseTree &other ) const;

            /*!
             *
             */
            virtual barrage::support::Any accept( Unique<ParserTreeVisitor> &visitor ) = 0;

            /*!
             *
             */
            virtual std::string getText() = 0;

            /*!
             *
             */
            virtual misc::Interval getSourceInterval() = 0;

        public:
            Shared<ParseTree> parent;
            SharedVec<ParseTree> children;
        };
    }
}
