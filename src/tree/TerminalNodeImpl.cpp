#include "../barrage-common.hxx"

#include "misc/Interval.hxx"
#include "tree/ParseTreeVisitor.hxx"
#include "RuleContext.hxx"
#include "Token.hxx"

#include "tree/TerminalNodeImpl.hxx"

namespace barrage {
    namespace tree {
        TerminalNodeImpl::TerminalNodeImpl( Shared<Token> a_symbol ) : symbol( a_symbol ) {
        }

        Shared<Token> TerminalNodeImpl::getSymbol() const {
            return symbol;
        }

        void TerminalNodeImpl::setParent( Shared<RuleContext> &a_parent ) {
            parent.swap( a_parent );
        }
    }
}