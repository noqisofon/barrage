#pragma once

#include "tree/ParseTree.hxx"

namespace barrage {
    namespace tree {
        /*!
         *
         */
        class TerminalNode : public ParseTree {
        public:
            virtual ~TerminalNode() {}

        public:
            /*!
             *
             */
            virtual Unique<Token> getSymbol() = 0;

            /*!
             *
             */
            virtual void setParent(Shared<RuleContext> &context) = 0;

            /*!
             *
             */
            virtual misc::Interval getSourceInterval() override;

            /*!
             *
             */
            virtual barrage::support::Any accept(Unique<ParseTreeVisitor>& visitor) override;
        };
    }
}
