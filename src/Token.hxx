#pragma once

#include "barrage-common.hxx"

#include "InputStream.hxx"

namespace barrage {
    /*!
     *
     */
    class BARRAGE_CPP_PUBLIC Token {
    public:
       /*!
        *
        */
        virtual ~Token();

    public:
       /*!
        *
        */
        virtual std::string getText() const = 0;

        /*!
         *
         */
        virtual std::size_t getType() const = 0;

        /*!
         *
         */
        virtual std::size_t getLine() const = 0;

        /*!
         *
         */
        virtual std::size_t getCharPositionInLine() const = 0;

        /*!
         *
         */
        virtual std::size_t getChannel() const = 0;

        /*!
         *
         */
        virtual std::size_t getTokenIndex() const = 0;

        /*!
         *
         */
        virtual std::size_t getStartIndex() const = 0;

        /*!
         *
         */
        virtual std::size_t getStopIndex() const = 0;

        /*!
         *
         */
        virtual Unique<TokenSource> getTokenSource() const = 0;

        /*!
         *
         */
        virtual Unique<CharStream> getInputStream() const = 0;

        /*!
         *
         */
        virtual std::string toString() const = 0;

    public:
        static const std::size_t   INVALID_TYPE = 0;

        static const std::size_t   EPSILON = static_cast<std::size_t>( -2 );
        static const std::size_t   MIN_USER_TOKEN_TYPE = 1;
        static const std::size_t   EOS = InputStream::EOS;

        static const std::size_t   DEFAULT_CHANNEL = 0;
        static const std::size_t   HIDDEN_CHANNEL = 1;

        /*!
         *
         * \see Token#getChannel()
         */
        static const std::size_t   MIN_USER_CHANNEL_VALUE = 2;
    };
}