#pragma once

#include "barrage-common.hxx"

namespace barrage {
    class BARRAGE_CPP_PUBLIC InputStream {
    public:
        /*!
         *
         */
        virtual ~InputStream();

    public:
       /*!
        *
        */
        virtual void consume() = 0;

        /*!
         *
         */
        virtual std::size_t LA( ssize_t an_index ) = 0;

        /*!
         *
         */
        virtual ssize_t mark() = 0;

        /*!
         *
         */
        virtual void release( ssize_t marker ) = 0;

        /*!
         *
         */
        virtual std::size_t index() = 0;

        /*!
         *
         */
        virtual void seek( ssize_t an_index ) = 0;

        /*!
         *
         */
        virtual std::size_t size() = 0;

        /*!
         *
         */
        virtual std::string getSourceName() const = 0;

    public:
        static const std::size_t  EOS = static_cast<std::size_t>( -1 );
    };
}