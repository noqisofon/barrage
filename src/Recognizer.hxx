#pragma once

#include "barrage-common.hxx"
#include "Token.hxx"
#include "ProxyErrorListener.hxx"

namespace barrage {
    class BARRAGE_CPP_PUBLIC Recognizer {
    public:

        Recognizer();
        Recognizer( Recognizer const & ) = delete;

        virtual ~Recognizer();

        Recognizer &operator = ( Recognizer const & ) = delete;

    public:
       /*!
        *
        */
        virtual Vec<std::string> const &getTokenNames() const = 0;
        /*!
         *
         */
        virtual Vec<std::string> const &getRuleNames() const = 0;

        /*!
         *
         */
        virtual dfa::Vocablary const &getVocablary() const;

        /*!
         *
         */
        virtual std::map<std::string, std::size_t> getTokenTypeMap();

        /*!
         *
         */
        virtual std::map<std::string, std::size_t> getRuleIndexMap();

        /*!
         *
         */
        virtual size_t getTokenType( const std::string &token_name );

        /*!
         *
         */
        virtual const Vec<std::uint16_t> getSerializeAtn() const {
            throw "there is no serialized ATN";
        }

        /*!
         *
         */
        virtual std::string getGrammarFileName() const = 0;

        /*!
         *
         */
        template <class _Type>
        Unique<_Type> getInterpreter() const {
            return dynamic_cast<_Type *>( interpreter );
        }

        /*!
         *
         */
        void setInterpreter( atn::ATNSimulator *interpreter );

        /*!
         *
         */
        virtual std::string getErrorHeader( RecoginitionException &ex );

        /*!
         *
         */
        virtual std::string getTokenErrorDisplay( Unique<Token> &a_token );

        /*!
         *
         */
        virtual Shared<InputStream> getInputStream() const = 0;

        /*!
         *
         */
        virtual void setInputStream( Shared<InputStream> &an_input_stream ) = 0;
    };
}