#pragma once

#include <algorithm>
#include <atomic>
#include <cassert>
#include <cstdarg>
#include <cstdint>
#include <exception>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifdef _WIN32

#   ifdef _MSC_VER
#       pragma warning (disable: 4250)    // Class inherits by dominance.
#       pragma warning (disable: 4512)    // assignment operator could be generated

#       if _MSC_VER < 1900
#           pragma warning (disable: 4127)    // conditional expression is constant
#       endif  /* _MSC_VER < 1900 */

#   endif  /* def _MSC_VER */

/*!
 * \def BARRAGE_USE_GUID_WINDOWS
 *
 * Windows の GUID 生成 API を使用することを表します。
 */
#   define BARRAGE_USE_GUID_WINDOWS

namespace barrage {
#   ifdef _WIN64
    typedef     __int64                     ssize_t;
#   else
    typedef     __int32                     ssize_t;
#   endif  /* def _WIN64 */

#   if _MSC_VER >= 1900 && _MSC_VER < 2000
    // VS 2015 has a known bug when using std::codecvt_utf8<char32_t>
    // so we have to temporarily use __int32 instead.
    //
    //      via: https://connect.microsoft.com/VisualStudio/feedback/details/1403302/unresolved-external-when-using-codecvt-utf8
    //
    typedef     std::basic_string<__int32>  i32string;

    typedef     i32string                   utf32string;
#   else
    typedef     std::u32string              utf32string;
#   endif  /* _MSC_VER >= 1900 && _MSC_VER < 2000 */
}

#   ifdef BARRAGE_CPP_EXPORTS
#       define      BARRAGE_CPP_PUBLIC      __declspec(dllexport)
#   else
#       ifdef BARRAGE_CPP_STATIC
#           define  BARRAGE_CPP_PUBLIC
#       else
#           define  BARRAGE_CPP_PUBLIC      __declspec(dllimport)
#       endif  /* BARRAGE_CPP_STATIC */
#   endif  /* def BARRAGE_CPP_EXPORTS */

#   if defined(_MSC_VER) && !defined(__clang__)
// clang-cl should excape this to prevent [ignored-attributes].
namespace std {
    class BARRAGE_CPP_PUBLIC exception;    // Prevents warning C4275 from MSVC.
}
#   endif  /* defined(_MSC_VER) && !defined(__clang__) */

#else

namespace barrage {
    typedef     std::u32string              utf32string;
}

#   define BARRAGE_USE_GUID_LIBUUID

#   if __GNUC__ >= 6
#       define  BARRAGE_CPP_PUBLIC      __attribute__ ((visibility ("default")))
#   else
#       define  BARRAGE_CPP_PUBLIC
#   endif  /* __GNUC__ >= 6 */

#endif  /*  _WIN32 */

#include "support/guid.h"
#include "support/declarations.h"

#if !defined(HAS_NOEXCEPT)

#   if defined(__clang__)
#       if __has_feature(cxx_noexcept)
#           define HAS_NOEXCEPT
#       endif  /* __has_feature(cxx_noexcept) */
#   else
#       if defined(__GXX_EXPRERIMENTAL_CXX0X__) && __GNUC__ * 10 + __GNUC_MINIR__ >= 46 || \
           defined(_MSC_FULL_VER) && _MSC_FULL_VER >= 190023026
#           define HAS_NOEXCEPT
#       endif  /*  */
#   endif  /* defined(__clang__) */

#   ifdef HAS_NOEXCEPT
#       define BARRAGE_NO_EXCEPT noexcept
#   else
#      define BARRAGE_NO_EXCEPT
#   endif  /* def HAS_NOEXCEPT */

#endif  /* !defined(HAS_NOEXCEPT) */

#define BARRAGE_INVALID_INDEX ( std::numeric_limits<std::size_t>::max() )
namespace barrage {
    template <class _Type> using Unique = std::unique_ptr<_Type>;
    template <class _Type> using Shared = std::shared_ptr<_Type>;

    template <class _Element> using Vec = std::vector<_Element>;
    template <class _Element> using SharedVec = std::vector<Shared<_Element>>;
}