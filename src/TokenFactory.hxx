#pragma once

#include "barrage-common.hxx"

namespace barrage {
    /*!
     *
     */
    template <typename _Symbol>
    class  BARRAGE_CPP_PUBLIC TokenFactory {
    public:
        virtual ~TokenFactory() {}

    public:
       /*!
        *
        */
        virtual Unique<_Symbol> create( std::pair<Unique<TokenSource>, Unique<CharStream>> &source,
                                     std::size_t type,
                                     const std::string &text,
                                     std::size_t chennel,
                                     std::size_t start,
                                     std::size_t stop,
                                     std::size_t line,
                                     std::size_t char_position_in_line ) = 0;
       /*!
        *
        */
        virtual Unique<_Symbol> create( std::size_t type, const std::string &text ) = 0;
    };
}