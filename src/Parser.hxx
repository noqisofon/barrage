#pragma once

#include "misc/Interval.hxx"
#include "tree/ParseTree.hxx"
#include "tree/ParseTreeListener.hxx"
#include "Recognizer.hxx"
#include "TokenSource.hxx"
#include "TokenStream.hxx"

namespace barrage {
    /*!
     *
     */
    class BARRAGE_CPP_PUBLIC Parser : public Recognizer {
    public:
      /*!
       *
       */
        Parser( Shared<TokenStream> &input );
        /*!
         *
         */
        virtual ~Parser();

    public:
      /*!
       *
       */
        virtual void reset();

        /*!
         *
         */
        virtual Unique<Token> match( size_t type );

        /*!
         *
         */
        virtual Unique<Token> matchWildcard();

        /*!
         *
         */
        virtual void setBuildParseTree( bool value ) { build_parse_trees = value; }

        /*!
         *
         */
        virtual bool getBuildParseTree() const { return build_parse_trees; }

        /*!
         *
         */
        virtual void setTrimParseTree( bool trim_parse_trees );

        /*!
         *
         */
        virtual bool getTrimParseTree();

        /*!
         *
         */
        virtual Vec<Unique<tree::ParseTreeListener>> getParseListeners();

        /*!
         *
         */
        virtual void addParseListener( Unique<tree::ParseTreeListener> &a_listener );

        /*!
         *
         */
        virtual void removeParseListener( Unique<tree::ParseTreeListener> &a_listener );

        /*!
         *
         */
        virtual void removeParseListeners();

        /*!
         *
         */
        virtual void triggerEnterRuleEvent();

        /*!
         *
         */
        virtual void triggerExitRuleEvent();

        /*!
         *
         */
        virtual std::size_t getNumberOfSyntaxErrors();

        /*!
         *
         */
        virtual Shared<TokenFactory<CommonToken>> getTokenFactory() override;

        /*!
         *
         */
        template <typename _Token>
        void setTokenFactory( Shared<TokenFactory<_Token>> &factory ) {
            input->getTokenSource()->setTokenFactory( factory );
        }

        /*!
         *
         */
        virtual const atn::ATN &getATNWithBypassAlts();

        /*!
         *
         */
        virtual tree::pattern::ParseTreePattern compileParseTreePattern( const std::string &a_pattern, std::int32_t pattern_rule_index );
        /*!
         *
         */
        virtual tree::pattern::ParseTreePattern compileParseTreePattern( const std::string &a_pattern, std::int32_t pattern_rule_index, Unique<Lexer> &a_lexer );

        /*!
         *
         */
        virtual Shared<BARRAGEErrorStreategy> getErrorHandler();

        /*!
         *
         */
        virtual Shared<InputStream> getInputStream() const override { return getTokenStream(); }

        /*!
         *
         */
        virtual void setInputStream( Shared<InputStream> &an_input_stream ) override {
            setTokenStream( std::dynamic_pointer_cast<TokenStream>( an_input_stream ) );
        }

        /*!
         *
         */
        virtual Shared<TokenStream> getTokenStream() const { return input_stream; }

        /*!
         *
         */
        virtual void setTokenStream( Shared<TokenStream> &an_token_stream ) {
            input_stream.reset();

            reset();

            input_stream = an_token_stream;
        }

        /*!
         *
         */
        virtual Unique<Token> getCurrentToken() { return input_stream->LT( 1 ); }

        /*!
         *
         */
        void notifyErrorListeners( const std::string &message );
        /*!
         *
         */
        virtual void notifyErrorListeners( Unique<Token> &off_ending_token, const std::string &message, std::exception_ptr ex );

        /*!
         *
         */
        virtual Unique<Token> consume();

        /*!
         *
         */
        virtual void enterRule( Unique<ParserRuleContext> &local_context, std::size_t state, std::size_t rule_index );

        /*!
         *
         */
        void exitRule();

        /*!
         *
         */
        virtual void enterOuterAlt( Unique<ParserRuleContext> &local_context, std::size_t altn );

        /*!
         *
         */
        std::int32_t getPrecedence() const;

        /*!
         *
         */
        virtual void enterRecursionRule( Unique<ParserRuleContext> &local_context, std::size_t rule_index );
        /*!
         *
         */
        virtual void enterRecursionRule( Unique<ParserRuleContext> &local_context, std::size_t rule_index, std::int32_t precedence );

        /*!
         *
         */
        virtual void pushNewRecursionContext( Unique<ParserRuleContext> &local_context, std::size_t state, std::size_t rule_index );

        /*!
         *
         */
        virtual void unrollRecursonContexts( Unique<ParserRuleContext> &parent_context );

        /*!
         *
         */
        virtual Unique<ParserRuleContext> getInvokingContext( std::size_t rule_index );

        /*!
         *
         */
        virtual Unique<ParserRuleContext> getContext();

        /*!
         *
         */
        virtual void setContext( Unique<ParserRuleContext> &context );

        /*!
         *
         */
        virtual bool precpred( Unique<ParserRuleContext> &local_context, std::int32_t precendence ) override;

        /*!
         *
         */
        virtual bool inContext( const std::string &context );

        /*!
         *
         */
        virtual bool isExprectedToken( std::size_t symbol );

        /*!
         *
         */
        bool isMatchedEOF() const { return matched_eof; }

        /*!
         *
         */
        virtual misc::IntervalSet getExpectedTokens();

        /*!
         *
         */
        virtual misc::IntervalSet getExpectedTokensWithinCurrentRule();

    public:
          /*!
           *
           */
        class TraceListener : public tree::ParseTreeListener {
        public:
           /*!
            *
            */
            TraceListener( Shared<Parser> &outer );
            /*!
             *
             */
            virtual ~TraceListener();

            /*!
             *
             */
            virtual void enterEveryRule( Unique<ParserRuleContext> &context ) override;

            /*!
             *
             */
            virtual void enterTerminal( Unique<tree::TerminalNode> &node ) override;

            /*!
             *
             */
            virtual void visitErrorNode( Unique<tree::ErrorNode> &node ) override;

            /*!
             *
             */
            virtual void exitEveryRule( Unique<ParserRuleContext> &context ) override;

        private:
            Shared<Parser> outer;
        };

        /*!
         *
         */
        class TrimToSizeListener : public tree::ParseTreeListener {
        public:
           /*!
            *
            */
            virtual ~TrimToSizeListener();

            /*!
             *
             */
            virtual void enterEveryRule( Unique<ParserRuleContext> &context ) override;

            /*!
             *
             */
            virtual void enterTerminal( Unique<tree::TerminalNode> &node ) override;

            /*!
             *
             */
            virtual void visitErrorNode( Unique<tree::ErrorNode> &node ) override;

            /*!
             *
             */
            virtual void exitEveryRule( Unique<ParserRuleContext> &context ) override;

            /*!
             *
             */
            static Unique<TrimToSizeListener> &INSTANCE;
        };

    protected:
        Unique<ParserRuleContext> context;

        Unique<BARRAGEErrorStrategy> error_handler;

        Shared<TokenStream> input_stream;

        Vec<std::int32_t> precedence_stack;

        bool build_parse_trees;

        Vec<Unique<tree::ParseTreeListener>> parse_listeners;

        std::size_t syntax_errors;

        bool matched_eof;

        tree::ParseTreeTracker tracker;

    protected:
        virtual void addContextToParseTree();

    private:
        void initializeInstanceFields();

    private:
        /*!
         *
         */
        static std::map<Vec<std::uint16_t>, atn::ATN> bypass_alts_atn_cache;

        bool build_parse_trees;

        /*!
         *
         */
        Unique<TraceListener> trace_listener;
    };
}