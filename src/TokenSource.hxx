#pragma once

#include "TokenFactory.hxx"

namespace barrage {
    /*!
     *
     */
    class TokenSource {
    public:
       /*!
        *
        */
        virtual ~TokenSource() {}

    public:
       /*!
        *
        */
        virtual Unique<Token> nextToken() = 0;

        /*!
         *
         */
        virtual std::size_t getLine() const = 0;

        /*!
         *
         */
        virtual std::size_t getCharPositionInLine() = 0;

        /*!
         *
         */
        virtual Unique<CharStream> getInputStream() = 0;

        /*!
         *
         */
        virtual std::string getSourceName() = 0;

        /*!
         *
         */
        template <typename _Symbol>
        void setTokenFactory( Unique<TokenFactory<_Symbol>> & /*factory*/ ) {}

        /*!
         *
         */
        virtual Shared<TokenFactory<CommonToken>> getTokenFactory() = 0;
    };
}
