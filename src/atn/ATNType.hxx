#pragma once

#include "barrage-common.hxx"

namespace barrage {
    namespace atn {
        /*!
         *
         */
        enum class ATNType {
            LEXER = 0,
            PARSER = 1
        };
    }
}