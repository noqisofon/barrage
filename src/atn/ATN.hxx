#pragma once

#include "RuleContext.hxx"

namespace barrage {
    namespace atn {
        /*!
         *
         */
        class BARRAGE_CPP_PUBLIC ATN {
        public:
           /*!
            *
            */
            ATN();
            /*!
             *
             */
            ATN( ATN &&other );
            /*!
             *
             */
            ATN( ATNType grammar_type, std::size_t max_token_type );

            virtual ~ATN();

        public:
           /*!
            *
            */
            ATN &operator = ( ATN &other ) BARRAGE_NO_EXCEPT;
            /*!
             *
             */
            ATN &operator = ( ATN &&other ) BARRAGE_NO_EXCEPT;

            /*!
             *
             */
            virtual misc::IntervalSet nextTokens( Shared<ATNState> &a_state, Shared<RuleContext> &context ) const;
            /*!
             *
             */
            virtual misc::IntervalSet const &nextTokens( Shared<ATNState> &a_state ) const;

            /*!
             *
             */
            virtual void addState( Shared<ATNState> &a_state );

            /*!
             *
             */
            virtual void removeState( Shared<ATNState> &a_state );

            /*!
             *
             */
            virtual std::int32_t defineDecisionState( Shared<DecisionState> &a_state );

            /*!
             *
             */
            virtual Shared<DecisionState> getDecisionState( std::size_t decision ) const;

            /*!
             *
             */
            virtual std::size_t getNumberOfDecisions() const;

            /*!
             *
             */
            virtual misc::IntervalSet getExpectedTokens( std::size_t state_number, Shared<RuleContext> &context ) const;

            /*!
             *
             */
            std::string toString() const;

        public:
            static const std::size_t INVALID_ALT_NUMBER = 0;

            Vec<Shared<ATNState>> states;

            Vec<Shared<DecisionState>> decision_states;

            Vec<Shared<RuleStartState>> rule_to_start_states;

            Vec<Shared<RuleStopState>> rule_to_stop_states;

            ATNType grammar_type;

            std::size_t max_token_type;

            Vec<std::size_t> rule_to_token_types;

            Vec<Shared<LexerAction>> lexer_actions;

            Vec<Shared<TokensStartState>> mode_to_start_states;

        private:
            mutable std::mutex mutex;
        };
    }
}
