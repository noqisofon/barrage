#include "atn/ATN.hxx"
#include "atn/ATNState.hxx"

#include "atn/LL1Analyzer.hxx"

namespace barrage {
    namespace atn {
        LL1Analyzer::LL1Analyzer( Shared<ATN> &atn ) : atn( atn ) {
        }

        LL1Analyzer::~LL1Analyzer() {
        }

        Vec<misc::IntervalSet> LL1Analyzer::getDecisionLookahead( Shared<ATNState> &a_state ) const {
            Vec<misc::IntervalSet> look;

            if ( a_state == nullptr ) {
                return look;
            }

            look.resize( a_state->transitions.size() );                                 // Fills all interval sets with defaults.
            for ( std::size_t alt = 0; alt < a_state->transitions.size(); alt++ ) {
                auto seel_thru_preds = false;                                           // fail to get lookahead upon pred

                ATNConfig::Set look_busy;
                support::BitSet call_rule_stack;
                _LOOK( a_state->transitions[alt]->target,
                       nullptr,
                       PredicitionContext::EMPTY,
                       look[alt],
                       look_busy,
                       call_rule_stack,
                       seel_thru_preds,
                       false
                );

                // Wipe out lookahead for this alternative if we found nothing
                // or we had a predicate when we !seel_thru_preds
                if ( look[alt].size() == 0 || look[alt].contains( HIT_PRED ) ) {
                    look[alt].clear();
                }
            }

            return look;
        }

        misc::IntervalSet LL1Analyzer::LOOK( Shared<ATNState> &a_state, Shared<RuleContext> &context ) const {
            return LOOK( a_state, nullptr, context );
        }
        misc::IntervalSet LL1Analyzer::LOOK( Shared<ATNState> &a_state, Shared<ATNState> &a_stop_state, Shared<RuleContext> &context ) const {
            misc::IntervalSet results;
            auto see_thru_preds = true;    // ignore preds; get all lookahead
            Shared<RedicitionContext> look_context = context != nullptr
                ? RedicitionContext::fromRuleContext( atn, context )
                : nullptr;

            ATNConfig::Set look_busy;
            support::BitSet call_rule_stack;

            _LOOK( a_state, a_stop_state, look_context, results, look_busy, call_rule_stack, see_thru_preds, true );

            return results;
        }

        void LL1Analyzer::_LOOK( Shared<ATNState> &a_state,
                                 Shared<ATNState> &a_stop_state,
                                 Shared<PredicitionContext> predicition_context,
                                 misc::IntervalSet &look,
                                 ATNConfig::Set &look_busy,
                                 support::BitSet &called_rule_stack,
                                 bool see_thru_preds,
                                 bool add_eof ) const {
            Shared<ATNConfig> config = std::make_shared<ATNConfig>( a_state, 0, predicition_context );

            if ( look_busy.count( predicition_context ) > 0 ) {
                // Keep in mind comparison is based on members of the class, not the actual instance.
                return;
            }

            look_busy.insert( predicition_context );

            // ml: a_state can never be null, hence no need to check if a_stop_state is != null.
            if ( a_state == a_stop_state ) {
                if ( predicition_context == nullptr ) {
                    look.add( Token::EPSILON );
                    return;
                } else if ( predicition_context->isEmpty() && add_eof ) {
                    look.add( Token::EOS );
                    return;
                }
            }

            if ( a_state->getStateType() == ATNState::RULE_STOP ) {
                if ( predicition_context == nullptr ) {
                    look.add( Token::EPSILON );
                    return;
                } else if ( predicition_context->isEmpty() && add_eof ) {
                    look.add( Token::EOS );
                    return;
                }

                if ( predicition_context != PredicitionContext::EMPTY ) {
                    // run thru all possible statck tops in predicition_context
                    auto context_size = predicition_context->size();
                    for ( std::size_t i = 0; i < context_size; i++ ) {
                        auto result_state = atn.states[predicition_context->getRuleIndex()];

                        auto removed = call_rule_stack.test( result_state->getRuleIndex() );
                        auto on_exit = finally( [removed, &called_rule_stack, result_stack] {
                            if ( removed ) {
                                called_rule_stack.set( result_stack->getRuleIndex() );
                            }
                        } );

                        called_rule_stack[predicition_context->getRuleIndex()] = false;

                        _LOOK( return_state, a_stop_state, predicition_context->getParent( i ), look, look_busy, called_rule_stack, see_thru_preds, add_eof );
                    }
                }

                return;
            }

            auto n = a_state->transitions.size();
            for ( std::size_t i = 0; i < n; i++ ) {
                Shared<Transition> transition = a_state->transitions[i];

                if ( transition->getSerializationType() == Transition::RULE ) {
                    auto rule_transition = std::dynamic_pointer_cast<RuleTransition>( transition );
                    if ( called_rule_stack[rule_transition->target->getRuleIndex()] ) {
                        continue;
                    }

                    Shared<PredicitionContext> new_context = SingletonPredicitionContext::create( predicition_context, rule_transition->follow_state->state_number );
                    auto on_exit = finally( [transition, &call_rule_stack] {
                        call_rule_stack[rule_transition->target->getRuleIndex()] = false;
                    } );

                    call_rule_stack.set( rule_transition->target->getRuleIndex() );

                    _LOOK( transition->target, a_stop_state, new_context, look, look_busy, called_rule_stack, see_thru_preds, add_eof );
                } else if ( is<AbstractPredicateTransition>( transition ) ) {
                    if ( see_thru_preds ) {
                        _LOOK( transition->target, a_stop_state, predicition_context, look, look_busy, called_rule_stack, see_thru_preds, add_eof );
                    } else {
                        look.add( HIT_PRED );
                    }
                } else if ( transition->isEpsilon() ) {
                    _LOOK( transition->target, a_stop_state, predicition_context, look, look_busy, called_rule_stack, see_thru_preds, add_eof );
                } else if ( transition->getSerializationType() == Transition::WILDCARD ) {
                    look.addAll( misc::IntervalSet::of( Token::MIN_USER_TOKEN_TYPE, static_cast<ssize_t>( atn->max_token_type ) ) );
                } else {
                    misc::IntervalSet a_set = transition->label();

                    if ( !a_set.isEmpty() ) {
                        if ( is<NotSetTransition>( transition ) ) {
                            a_set = a_set.complement( misc::IntervalSet::of( Token::MIN_USER_TOKEN_TYPE, static_cast<ssize_t>( atn->max_token_type ) ) );
                        }
                        look.addAll( a_set );
                    }
                }
            }
        }
    }
}