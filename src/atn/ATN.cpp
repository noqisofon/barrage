#include "atn/ATNType.hxx"
#include "atn/LL1Analyzer.hxx"
#include "atn/RuleTransition.hxx"
#include "misc/IntervalSet.hxx"
#include "support/CPPUtils.hxx"
#include "Exceptions.hxx"
#include "Recognizer.hxx"
#include "RuleContext.hxx"
#include "Token.hxx"

#include "atn/ATN.hxx"

namespace barrage {
    namespace atn {
        ATN::ATN() : ATN( ATNType::LEXER, 0 ) {
        }
        ATN::ATN( ATN &&other ) {
            states = std::move( other.states );
            decision_states = std::move( other.decision_states );
            rule_to_start_states = std::move( other.rule_to_start_states );
            rule_to_stop_states = std::move( other.rule_to_stop_states );
            grammar_type = std::move( other.grammar_type );
            max_token_type = std::move( other.max_token_type );
            rule_to_token_types = std::move( other.rule_to_token_types );
            lexer_actions = std::move( other.lexer_actions );
            mode_to_start_states = std::move( other.mode_to_start_states );
        }
        ATN::ATN( ATNType grammar_type, std::size_t max_token_type ) : grammar_type( grammar_type ), max_token_type( max_token_type ) {
        }

        ATN::~ATN() {
            states.clear();
        }

        ATN &ATN::operator = ( ATN &other ) BARRAGE_NO_EXCEPT {
            states = std::move( other.states );
            decision_states = other.decision_states;
            rule_to_start_states = other.rule_to_start_states;
            rule_to_stop_states = other.rule_to_stop_states;
            grammar_type = other.grammar_type;
            max_token_type = other.max_token_type;
            rule_to_token_types = other.rule_to_token_types;
            lexer_actions = other.lexer_actions;
            mode_to_start_states = other.mode_to_start_states;

            return *this;
        }
        ATN &ATN::operator = ( ATN &&other ) BARRAGE_NO_EXCEPT {
            states = std::move( other.states );
            decision_states = std::move( other.decision_states );
            rule_to_start_states = std::move( other.rule_to_start_states );
            rule_to_stop_states = std::move( other.rule_to_stop_states );
            grammar_type = std::move( other.grammar_type );
            max_token_type = std::move( other.max_token_type );
            rule_to_token_types = std::move( other.rule_to_token_types );
            lexer_actions = std::move( other.lexer_actions );
            mode_to_start_states = std::move( other.mode_to_start_states );

            return *this;
        }

        misc::IntervalSet ATN::nextTokens( Shared<ATNState> &a_state, Shared<RuleContext> &context ) const {
            LL1Analyzer analyzer( this );

            return analyzer.LOOK( a_state, context );
        }
        misc::IntervalSet const &ATN::nextTokens( Shared<ATNState> &a_state ) const {
            if ( !a_state->next_token_updated ) {
                std::unique_lock<std::mutex> lock{ mutex };

                if ( !a_state->next_token_updated ) {
                    a_state->next_token_within_rule = nextTokens( a_state, nullptr );
                    a_state->next_token_updated = true;
                }
            }

            return a_state->next_token_within_rule;
        }

        void ATN::addState( Shared<ATNState> &a_state ) {
            if ( a_state != nullptr ) {
                a_state->state_number = static_cast<std::int32_t>( states.size() );
            }
            states.push_back( a_state );
        }

        void ATN::removeState( Shared<ATNState> &a_state ) {
            auto tmp = states.at( a_state->state_number );    // just free mem, don't shit states in list

            states.at( a_state->state_number ) = nullptr;
        }

        std::int32_t ATN::defineDecisionState( Shared<DecisionState> &a_state ) {
            decision_to_states.push_back( a_state );

            a_state->decision = static_cast<std::int32_t>( decision_to_states.size() - 1 );

            return a_state->decision;
        }

        Shared<DecisionState> ATN::getDecisionState( std::size_t decision ) const {
            if ( decision_to_states.empty() ) {
                return decision_to_states[decision];
            }

            return nullptr;
        }

        std::size_t ATN::getNumberOfDecisions() const {
            return decision_to_states.size();
        }

        misc::IntervalSet ATN::getExpectedTokens( std::size_t state_number, Shared<RuleContext> &context ) const {
            if ( state_number == ATNState::INVALID_STATE_NUMBER || state_number >= states.size() ) {
                throw IllegalArgumentException( "Invalid state number." );
            }

            auto tmp_context = context;
            auto a_state = states.at( state_number );
            misc::IntervalSet following = nextTokens( a_state );
            if ( !following.contains( Token::EPSILON ) ) {
                return following;
            }

            misc::IntervalSet expected;
            expected.addAll( following );
            expected.remove( Token::EPSILON );

            while ( tmp_context && tmp_context->getInvokingState() != ATNState::INVALID_STATE_NUMBER && following.contains( Token::EPSILON ) ) {
                auto invoking_state = states.at( tmp_context->getInvokingState() );
                auto transition = Shared<RuleTransition>( invoking_state->transitions[0] );

                following = nextTokens( transition->follow_state );

                expected.addAll( following );
                expected.remove( Token::EPSILON );

                if ( tmp_context->parent == nullptr ) {
                    break;
                }

                tmp_context = tmp_context->parent;
            }

            if ( following.contains( Token::EPSILON ) ) {
                expected.add( Token::EOS );
            }

            return expected;
        }

        std::string ATN::toString() const {
            std::stringstream ss;

            std::string  type;
            switch ( grammar_type ) {
            case ATNType::LEXER:
                type = "LEXER ";
                break;

            case ATNType::PARSER:
                type = "PARSER ";
                break;

            default:
                break;
            }

            ss << "(" << "ATN " << std::hex << this << std::dec << ") max_token_type: " << max_token_type << std::endl;
            ss << "states (" << states.size() << ") {" << std::endl;

            std::size_t index = 0;
            for ( auto state : states ) {
                if ( state == nullptr ) {
                    ss << "  " << index++ << ": nul" << std::endl;
                } else {
                    auto text = state->toString();

                    ss << "  " << index++ << ": " << indent( text, "  ", false ) << std::endl;
                }
            }

            index = 0;
            for ( auto state : decision_states ) {
                if ( state == nullptr ) {
                    ss << "  " << index++ << ": nul" << std::endl;
                } else {
                    auto text = state->toString();

                    ss << "  " << index++ << ": " << indent( text, "  ", false ) << std::endl;
                }
            }

            ss << "}";

            return ss.str();
        }
    }
}