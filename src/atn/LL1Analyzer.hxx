#pragma once

#include "Token.hxx"
#include "atn/ATNConfig.hxx"
#include "atn/PredictionContext.hxx"
#include "misc/IntervalSet.hxx"
#include "support/BitSet.hxx"

namespace barrage {
    namespace atn {
        /*!
         *
         */
        class LL1Analyzer {
        public:
           /*!
            *
            */
            LL1Analyzer( Shared<ATN> &atn );

            virtual ~LL1Analyzer();

        public:
           /*!
            *
            */
            virtual Vec<misc::IntervalSet> getDecisionLookahead( Shared<ATNState> &a_state ) const;

           /*!
            *
            */
            virtual misc::IntervalSet LOOK( Shared<ATNState> &a_state, Shared<RuleContext> &context ) const;
           /*!
            *
            */
            virtual misc::IntervalSet LOOK( Shared<ATNState> &a_state, Shared<ATNState> &a_stop_state, Shared<RuleContext> &context ) const;

        public:
            static const std::size_t HIT_PRED = Token::INVALID_TYPE;

        protected:
            virtual void _LOOK( Shared<ATNState> &a_state,
                                Shared<ATNState> &a_stop_state,
                                Shared<PredicitionContext> predicition_context,
                                misc::IntervalSet &look,
                                ATNConfig::Set &look_busy,
                                support::BitSet &called_rule_stack,
                                bool see_thru_preds,
                                bool add_eof ) const;

        private:
            const Shared<atn::ATN>  atn;
        };
    }
}