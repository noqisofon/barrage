#include "atn/ATN.hxx"
#include "atn/ATNDeserializationOptions.hxx"
#include "atn/ATNDeserializer.hxx"
#include "atn/ParserATNSimulator.hxx"
#include "atn/RuleStartState.hxx"
#include "atn/RuleTransion.hxx"
#include "dfa/DFA.hxx"
#include "misc/IntervalSet.hxx"
#include "tree/ErrorNodeImpl.hxx"
#include "tree/pattern/ParseTreePattern.hxx"
#include "tree/pattern/ParseTreePatternMatcher.hxx"
#include "tree/TerminalNode.hxx"
#include "BARRAGEErrorListener.hxx"
#include "DefaultErrorStrategy.hxx"
#include "Exceptions.hxx"
#include "Lexer.hxx"
#include "ParserRuleContext.hxx"

#include "Parser.hxx"

namespace barrage {
    Parser::Parser( Shared<TokenStream> &input_stream ) {
        initializeInstanceFields();
        setTokenStream( input_stream );
    }

    Parser::~Parser() {
        tracker.reset();
    }

    void Parser::reset() {
        if ( getInputStream() != nullptr ) {
            getInputStream()->seek( 0 );
        }

        error_handler->reset( this );

        matched_eof = false;
        syntax_errors = 0;

        setTrace( false );

        precedence_stack.clear();
        precedence_stack.push_back( 0 );

        context = nullptr;
        tracker.reset();

        auto interpreter = getInterpreter<atn::ATNSimulator>();
        if ( interpreter != nullptr ) {
            interpreter->reset();
        }
    }

    Unique<Token> Parser::match( size_t type_code ) {
        auto a_token = getCurrentToken();

        if ( a_token->getType() == type_code ) {
            if ( type_code == EOF ) {
                matched_eof = true;
            }
            error_handler->reportMatch( this );
            consume();
        } else {
            a_token = error_handler->recoverInline( this );
            if ( build_parse_trees && a_token->getTokenIndex() == BARRAGE_INVALID_INDEX ) {
                context->addChild( createErrorNode( a_token ) );
            }
        }

        return a_token;
    }

    Unique<Token> Parser::matchWildcard() {
        auto a_token = getCurrentToken();

        if ( a_token->getType() > 0 ) {
            error_handler->reportMatch( this );
            consume();
        } else {
            a_token = error_handler->recoverInline( this );
            if ( build_parse_trees && a_token->getTokenIndex() == BARRAGE_INVALID_INDEX ) {
                context->addChild( createErrorNode( a_token ) );
            }
        }

        return a_token;
    }

    void Parser::setTrimParseTree( bool trim_parse_trees ) {
    }

    Parser::TraceListener::TraceListener( Shared<Parser> &outer ) : outer( outer ) {
    }

    Parser::TraceListener::~TraceListener() {
    }

    void Parser::TraceListener::enterEveryRule( Unique<ParserRuleContext> &context ) {
        auto index = context->getRuleIndex();

        std::cout <<
            "enter   " << outer->getRuleNames()[index] <<
            ", LT(1)=" << outer->input_stream->LT( 1 )->getText() << std::endl;
    }

    void Parser::TraceListener::enterTerminal( Unique<tree::TerminalNode> &node ) {
        auto index = outer->getContext()->getRuleIndex();

        std::cout << "consume " << node->getSymbol() << " rule "
            << outer->getRuleNames()[index] << std::endl;
    }

    void Parser::TraceListener::visitErrorNode( Unique<tree::ErrorNode> & ) {
    }

    void Parser::TraceListener::exitEveryRule( Unique<ParserRuleContext> &context ) {
        auto index = context->getRuleIndex();

        std::cout <<
            "enter   " << outer->getRuleNames()[index] <<
            ", LT(1)=" << outer->input_stream->LT( 1 )->getText() << std::endl;
    }

    Parser::TrimToSizeListener::~TrimToSizeListener() {
    }

    void Parser::TrimToSizeListener::enterEveryRule( Unique<ParserRuleContext> & ) {
    }

    void Parser::TrimToSizeListener::enterTerminal( Unique<tree::TerminalNode> & ) {
    }

    void Parser::TrimToSizeListener::visitErrorNode( Unique<tree::ErrorNode> & ) {
    }

    void Parser::TrimToSizeListener::exitEveryRule( Unique<ParserRuleContext> &context ) {
        context->children.shrink_to_fit();
    }

    std::map<Vec<std::uint16_t>, atn::ATN> Parser::bypass_alts_atn_cache;
}
