#pragma once

#include "barrage-common.hxx"
#include "tree/ParseTree.hxx"


namespace barrage {

    class RuleContext : public tree::ParseTree {
     public:
        RuleContext();
        RuleContext(Unique<RuleContext>& parent, std::size_t invoking_state);

     public:
        /*!
         *
         */
        virtual void        setInvokingState(std::size_t value) { invoking_state = value; }

        /*!
         *
         */
        virtual std::size_t getInvokingState() const { return invoking_state; }

     private:
        std::size_t invoking_state;
    };
}
