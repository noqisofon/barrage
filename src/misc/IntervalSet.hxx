#pragma once

#include "misc/Interval.hxx"

namespace barrage {
    namespace misc {
        class  IntervalSet {
        public:
           /*!
            *
            */
            IntervalSet();
            /*!
             *
             */
            IntervalSet( const IntervalSet &other );

        private:
            explicit IntervalSet( Vec<Interval> &&intervals );

        public:
            /*!
             *
             */
            bool operator== ( const IntervalSet &other ) const;

           /*!
            *
            */
            static IntervalSet of( ssize_t e );
            /*!
             *
             */
            static IntervalSet of( ssize_t a, ssize_t b );

        public:
            /*!
             *
             */
            std::size_t size() const;

            /*!
             *
             */
            Vec<ssize_t> toList() const;

            /*!
             *
             */
            std::set<ssize_t> toSet() const;

            /*!
             *
             */
            ssize_t get( std::size_t i ) const;

            /*!
             *
             */
            void remove(std::size_t e);
            /*!
             *
             */
            void remove(ssize_t e);

        public:
            /*!
             *
             */
            void clear();

            /*!
             *
             */
            void add( ssize_t e );
            /*!
             *
             */
            void add( ssize_t a, ssize_t b );
            /*!
             *
             */
            void add( const Interval &addition );

            /*!
             *
             */
            IntervalSet &addAll( const IntervalSet &a_set );

            /*!
             *
             */
            IntervalSet complement( ssize_t mine, ssize_t maxe ) const;
            /*!
             *
             */
            IntervalSet complement( const IntervalSet &vocabulary ) const;

            /*!
             *
             */
            IntervalSet subtract( const IntervalSet &other ) const;

            /*!
             *
             */
            static IntervalSet subtract( const IntervalSet &left, const IntervalSet &right );

            /*!
             *
             */
            IntervalSet orOther( const IntervalSet &other ) const;

            /*!
             *
             */
            IntervalSet andOther( const IntervalSet &other ) const;

            /*!
             *
             */
            bool contains( std::size_t e ) const;
            /*!
             *
             */
            bool contains( ssize_t e ) const;

            /*!
             *
             */
            bool isEmpty() const;

            /*!
             *
             */
            ssize_t getSingleElement() const;

            /*!
             *
             */
            ssize_t getMaxElement() const;

            /*!
             *
             */
            ssize_t getMinElement() const;

            /*!
             *
             */
            Vec<Interval> const &getIntervals() const;

            /*!
             *
             */
            std::string toString() const;
            /*!
             *
             */
            std::string toString( bool element_are_char ) const;
            /*!
             *
             */
            std::string toString( const dfa::Vocabulary &vocabulary ) const;

            /*!
             *
             */
            std::size_t hashCode() const;

        protected:
            /*!
             *
             */
            std::string elementName( const dfa::Vocabulary &vocabulary, ssize_t a ) const;

        private:
            Vec<Interval> intervals;
        };
    }
}

namespace std {
    using barrage::misc::IntervalSet;

    template <> struct hash<IntervalSet> {
        size_t operator() ( const IntervalSet &other ) const {
            return other.hashCode();
        }
    };
}
