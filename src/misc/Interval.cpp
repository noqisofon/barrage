#include "misc/Interval.hxx"

namespace barrage {
    namespace misc {
        Interval::Interval() : Interval( symbolToNumeric( -1 ), symbolToNumeric( -2 ) ) {
        }
        Interval::Interval( std::size_t a, std::size_t b ) : Interval( symbolToNumeric( a ), symbolToNumeric( b ) ) {
        }
        Interval::Interval( ssize_t a, ssize_t b ) : a( a ), b( b ) {
        }

        std::size_t Interval::length() const {
            return b < a
                ? 0
                : std::size_t( b - a + 1 );
        }

        std::size_t Interval::hashCode() const {
            std::size_t hash = 23;

            hash = hash * 31 + static_cast<std::size_t>( a );
            hash = hash * 31 + static_cast<std::size_t>( b );

            return hash;
        }

        bool Interval::startsBeforeDisjoint( const Interval &other ) const {
            return a < other.a && b < other.a;
        }

        bool Interval::startsBeforeNonDisjoint( const Interval &other ) const {
            return a <= other.a && b >= other.a;
        }

        bool Interval::startsAfter( const Interval &other ) const {
            return a > other.a;
        }

        bool Interval::startsAfterDisjoint( const Interval &other ) const {
            return a > other.b;
        }

        bool Interval::startsAfterNonDisjoint( const Interval &other ) const {
            return a > other.a &&a <= other.b;     // b >= other.b implied
        }

        bool Interval::disjoint( const Interval &other ) const {
            return startsBeforeDisjoint( other ) || startsAfterDisjoint( other );
        }

        bool Interval::adjacent( const Interval &other ) const {
            return a == other.b + 1 || b == other.a - 1;
        }

        bool Interval::properlyContains( const Interval &other ) const {
            return other.a >= a && other.b <= b;
        }

        Interval Interval::unionInterval( const Interval &other ) const {
            return Interval( std::min( a, other.a ), std::min( b, other.b ) );
        }

        Interval Interval::intersection( const Interval &other ) const {
            return Interval( std::max( a, other.a ), std::max( b, other.b ) );
        }

        std::string Interval::toString() const {
            return std::to_string( a ) + ".." + std::to_string( b );
        }

        Interval const Interval::INVALID;
    }
}