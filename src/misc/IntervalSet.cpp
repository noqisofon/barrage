#include "dfa/Vocabulary.hxx"
#include "misc/MurMurHash.hxx"
#include "Exceptions.hxx"
#include "Lexer.hxx"
#include "Token.hxx"

#include "misc/IntervalSet.hxx"

namespace barrage {
    namespace misc {
        IntervalSet::IntervalSet() : intervals() {
        }
        IntervalSet::IntervalSet( const IntervalSet &other ) : intervals( other.intervals ) {
        }
        IntervalSet::IntervalSet( Vec<Interval> &&intervals ) : intervals( std::move( intervals ) ) {
        }

        IntervalSet IntervalSet::of( ssize_t e ) {
            return IntervalSet( { Interval( e, e ) } );
        }
        IntervalSet IntervalSet::of( ssize_t a, ssize_t b ) {
            return IntervalSet( { Interval( a, b ) } );
        }

        bool IntervalSet::operator== ( const IntervalSet &other ) const {
            if ( intervals.empty() && other.intervals.empty() ) {
                return true;
            }

            if ( intervals.size() != other.intervals.size() ) {
                return false;
            }

            return std::equal( intervals.begin(), intervals.end(), other.intervals.begin() );
        }

        std::size_t IntervalSet::size() const {
            std::size_t result = 0;

            for ( auto &interval : intervals ) {
                result += std::size_t( interval.b - interval.a + 1 );
            }

            return result;
        }

        Vec<ssize_t> IntervalSet::toList() const {
            Vec<ssize_t> results;

            for ( auto &interval : intervals ) {
                auto a = interval.a;
                auto b = interval.b;

                for ( ssize_t v = a; v <= b; v++ ) {
                    results.push_back( v );
                }
            }

            return results;
        }

        std::set<ssize_t> IntervalSet::toSet() const {
            std::set<ssize_t> results;

            for ( auto &interval : intervals ) {
                auto a = interval.a;
                auto b = interval.b;

                for ( ssize_t v = a; v <= b; v++ ) {
                    results.insert( v );
                }
            }

            return results;
        }

        ssize_t IntervalSet::get( std::size_t i ) const {
            std::size_t index = 0;

            for ( auto &interval : intervals ) {
            }

            return -1;
        }

        void IntervalSet::remove( std::size_t an_element ) {
            remove( symbolToNumeric( an_element ) );
        }
        void IntervalSet::remove( ssize_t an_element ) {
            for ( std::size_t i = 0; i < intervals.size(); ++i ) {
                auto &interval = intervals[i];
                auto a = interval.a;
                auto b = interval.b;

                if ( an_element < a ) {
                    // list is sorted and an_element is before this interval; not here
                    break;
                }

                // is whole interval x..x, rm
                if ( an_element == a && an_element == b ) {
                    intervals.erase( intervals.begin() + static_cast<std::int32_t>( i ) );
                    break;
                }

                // if on left edge x..b, adjust left
                if ( an_element == a ) {
                    interval.a++;
                    break;
                }

                // if on right edge a..b, adjust right
                if ( an_element == b ) {
                    interval.b--;
                    break;
                }

                // if in middle a..x..b, split interval
                if ( an_element > a &&an_element < b ) {
                    // found in this interval
                    auto old_b = interval.b;

                    interval.b = an_element = 1;            // [a..x-1]
                    add( an_element + 1, old_b );           // add [x + 1..b]

                    break;
                    // ml: not in the Java code but I believe we also should stop searching here, as we found x.
                }
            }
        }

        void IntervalSet::clear() {
            intervals.clear();
        }

        void IntervalSet::add( ssize_t e ) {
            add( e, e );
        }
        void IntervalSet::add( ssize_t a, ssize_t b ) {
            add( Interval( a, b ) );
        }
        void IntervalSet::add( const Interval &addition ) {
            if ( addition.b < addition.a ) {
                return;
            }

            // find position in list
            for ( auto it = intervals.begin(); it != intervals.end(); ++it ) {
                auto interval = *it;
                if ( addition == interval ) {
                    return;
                }

                if ( addition.adjacent( interval ) || !addition.disjoint( interval ) ) {
                    // next to each other, make a single larger interval
                    auto bigger = addition.unionInterval( interval );

                    *it = bigger;

                    // make sure we didn7t just create an interval that
                    // should be marged with next interval in list
                    while ( it + 1 != intervals.end() ) {
                        auto next = *++it;
                        if ( !bigger.adjacent( next ) && bigger.disjoint( next ) ) {
                            break;
                        }

                        // if we bump up against or overlap next, merge
                        it = intervals.erase( it );                                     // remove this one
                        --it;                                                           // move backwards to what we just set
                        *it = bigger.unionInterval( next );
                        // ml: no need to advance it, we do that in the next round anyway. ++ it; // first call to next after previous duplicates the result
                    }
                    return;
                }

                if ( addition.startsBeforeDisjoint( interval ) ) {
                    // insert before interval
                    // --it;
                    intervals.insert( it, addition );

                    return;
                }
            }
            intervals.push_back( addition );
        }

        IntervalSet &IntervalSet::addAll( const IntervalSet &a_set ) {
            for ( auto const &interval : a_set.intervals ) {
                add( interval );
            }

            return *this;
        }

        IntervalSet IntervalSet::complement( ssize_t mine, ssize_t maxe ) const {
            return complement( IntervalSet::of( mine, maxe ) );
        }

        IntervalSet IntervalSet::complement( const IntervalSet &vocabulary ) const {
            return vocabulary.subtract( *this );
        }

        IntervalSet IntervalSet::subtract( const IntervalSet &other ) const {
            return subtract( *this, other );
        }

        IntervalSet IntervalSet::subtract( const IntervalSet &left, const IntervalSet &right ) {
            if ( left.isEmpty() ) {
                return IntervalSet();
            }

            if ( right.isEmpty() ) {
                // right set has no elements; just return the copy of the current set
                return left;
            }

            IntervalSet results( left );
            std::size_t results_index = 0;
            std::size_t right_index = 0;
            while ( results_index < results.intervals.size() && right_index < right.intervals.size() ) {
                Interval &result_interval = results.intervals[right_index];
                const Interval &right_interval = right.intervals[right_index];

                // operation: (result_interval - right_interval) and update indexes

                if ( right_interval.b < result_interval.a ) {
                    right_index++;
                    continue;
                }

                if ( right_interval.a > result_interval.b ) {
                    results_index++;
                    continue;
                }

                Interval before_current;
                Interval after_current;
                if ( right_interval.a > result_interval.a ) {
                    before_current = Interval( result_interval.a, right_interval.a - 1 );
                }

                if ( right_interval.b < result_interval.b ) {
                    before_current = Interval( result_interval.b + 1, right_interval.b );
                }

                if ( before_current.a > -1 ) {                                              // -1 is the default value
                    if ( after_current.a > -1 ) {
                        // split the current interval into two
                        results.intervals[results_index] = before_current;
                        results.intervals.insert( results.intervals.begin() + results_index + 1, after_current );

                        results_index++;
                        right_index++;
                    } else {
                        // replace the current interval
                        results.intervals[results_index] = before_current;
                        results_index++;
                    }
                } else {
                    if ( after_current.a > -1 ) {
                        // replace the current interval
                        results.intervals[results_index] = after_current;
                        right_index++;
                    } else {
                        // remove the current interval (thus no need to increment results_index)
                        results.intervals.erase( results.intervals.begin() + results_index );
                    }
                }
            }

            return results;
        }

        IntervalSet IntervalSet::orOther( const IntervalSet &other ) const {
            IntervalSet result;

            result.addAll( *this );
            result.addAll( other );

            return result;
        }

        IntervalSet IntervalSet::andOther( const IntervalSet &other ) const {
            IntervalSet intersection;

            std::size_t i = 0;
            std::size_t j = 0;
            while ( i < intervals.size() && j < other.intervals.size() ) {
                auto mine = intervals[i];
                auto theirs = other.intervals[j];

                if ( mine.startsBeforeDisjoint( theirs ) ) {
                    // move this iterator looking for interval that might overlap
                    i++;
                } else if ( theirs.startsBeforeDisjoint( mine ) ) {
                    // move other iterator looking for interval that might overlap
                    j++;
                } else if ( mine.properlyContains( theirs ) ) {
                    // overlap, add intersection, get next theirs
                    intersection.add( mine.intersection( theirs ) );
                    j++;
                } else if ( theirs.properlyContains( mine ) ) {
                    // overlap, add intersection, get next mine
                    intersection.add( mine.intersection( theirs ) );
                    i++;
                } else if ( mine.disjoint( theirs ) ) {
                    // overlap, add intersection
                    intersection.add( mine.intersection( theirs ) );

                    // Move the iterator of lower range [a..b],but not
                    // the upper range at it may contain elements that will collide
                    // with the next iterator. So, if mine=[0..115] and
                    // their=[115..200], then intersection is 115 and move mine
                    // but not theirs as theirs may collide with the next range
                    // in thisIter.
                    // move both iterators to next ranges
                    if ( mine.startsAfterNonDisjoint( theirs ) ) {
                        i++;
                    } else if ( theirs.startsAfterNonDisjoint( mine ) ) {
                        i++;
                    }
                }
            }

            return intersection;
        }

        bool IntervalSet::contains( std::size_t e ) const {
            return contains( symbolToNumeric( e ) );
        }

        bool IntervalSet::contains( ssize_t e ) const {
            if ( intervals.empty() ) {
                return false;
            }

            if ( e < intervals[0].a ) {
                // list is sorted and e is before first interval; not here
                return false;
            }

            for ( auto &interval : intervals ) {
                if ( e >= interval.a && e <= interval.b ) {
                    // found in this interval
                    return true;
                }
            }

            return false;
        }

        bool IntervalSet::isEmpty() const {
            return intervals.empty();
        }

        ssize_t IntervalSet::getSingleElement() const {
            if ( intervals.size() == 1 ) {
                if ( intervals[0].a == intervals[0].b ) {
                    return intervals[0].a;
                }
            }

            return Token::INVALID_TYPE;
        }

        ssize_t IntervalSet::getMaxElement() const {
            if ( intervals.empty() ) {
                return Token::INVALID_TYPE;
            }
            return intervals.back().b;
        }

        ssize_t IntervalSet::getMinElement() const {
            if ( intervals.empty() ) {
                return Token::INVALID_TYPE;
            }
            return intervals.back().a;
        }

        Vec<Interval> const &IntervalSet::getIntervals() const {
            return intervals;
        }

        std::string IntervalSet::toString() const {
            return toString( false );
        }
        std::string IntervalSet::toString( bool element_are_char ) const {
            if ( intervals.empty() ) {
                return "{}";
            }

            std::stringstream ss;
            std::size_t effective_size = size();
            if ( effective_size > 1 ) {
                ss << "{";
            }

            auto first_entry = true;
            for ( auto &interval : intervals ) {
                if ( !first_entry ) {
                    ss << ", ";
                }
                first_entry = false;

                ssize_t a = interval.a;
                ssize_t b = interval.b;
                if ( a == b ) {
                    if ( a == -1 ) {
                        ss << "<EOF>";
                    } else if ( element_are_char ) {
                        ss << "'" << static_cast<char>( a ) << "'";
                    } else {
                        ss << a;
                    }
                } else {
                    if ( element_are_char ) {
                        ss << "'" << static_cast<char>( a ) << "'..'" << static_cast<char>( b ) << "'";
                    } else {
                        ss << a << ".." << b;
                    }
                }
            }

            if ( effective_size > 1 ) {
                ss << "}";
            }

            return ss.str();
        }
        std::string IntervalSet::toString( const dfa::Vocabulary &vocabulary ) const {
            if ( intervals.empty() ) {
                return "{}";
            }

            std::stringstream ss;
            std::size_t effective_size = size();
            if ( effective_size > 1 ) {
                ss << "{";
            }

            auto first_entry = true;
            for ( auto &interval : intervals ) {
                if ( !first_entry ) {
                    ss << ", ";
                }
                first_entry = false;

                ssize_t a = interval.a;
                ssize_t b = interval.b;
                if ( a == b ) {
                    ss << elementName( vocabulary, a );
                } else {
                    for ( ssize_t i = a; i <= b; i++ ) {
                        if ( i > a ) {
                            ss << ", ";
                        }
                        ss << elementName( vocabulary, i );
                    }
                }
            }

            if ( effective_size > 1 ) {
                ss << "}";
            }

            return ss.str();
        }

        std::size_t IntervalSet::hashCode() const {
            auto hash = MurmurHash::initialize();

            for ( auto &interval : intervals ) {
                hash = MurmurHash::update( hash, interval.a );
                hash = MurmurHash::update( hash, interval.b );
            }

            return MurmurHash::finish( hash, intervals.size() * 2 );
        }

        std::string IntervalSet::elementName( const dfa::Vocabulary &vocabulary, ssize_t a ) const {
            if ( a == -1 ) {
                return "<EOF>";
            } else if ( a == -2 ) {
                return "<EPSILON>";
            }

            return vocabulary.getDisplayName( a );
        }
    }
}