#pragma once

#include "barrage-common.hxx"

namespace barrage {
    namespace misc {
        /*!
         *
         */
        class MurmurHash {
        public:
            /*!
             *
             */
            static std::size_t initialize();
            /*!
             *
             */
            static std::size_t initialize( std::size_t seed );

            /*!
             *
             */
            static std::size_t update( ssize_t hash, std::size_t value );

            /*!
             *
             */
            static std::size_t finish(std::size_t hash, std::size_t entry_count);

        private:
            static const std::size_t DEFAULT_SEED = 0;
        };
    }
}
