#include "misc/MurmurHash.hxx"

#if defined(_MSC_VER)

#   define  FORCE_INLINE    __forceinline

#   include <stdlib.h>

#   define  ROTL32(_x_, _y_)     _rotl(_x_, _y_)
#   define  ROTL64(_x_, _y_)     _rotl64(_x_, _y_)

#   define  BIG_CONSTANT(_x_)    (_x_)

#else  /* defined(_MSC_VER) */

// Other compilers

#   define  FORCE_INLINE    inline __attribute__ ((always_inline))

inline std::uint32_t rotl32( std::uint32_t x, std::uint8_t r ) {
    return ( x << r ) | ( x >> ( 32 - r ) );
}

inline std::uint32_t rotl64( std::uint64_t x, std::uint8_t r ) {
    return ( x << r ) | ( x >> ( 64 - r ) );
}

#   define  ROTL32(_x_, _y_)     rotl(_x_, _y_)
#   define  ROTL64(_x_, _y_)     rotl64(_x_, _y_)

#   define  BIG_CONSTANT(_x_)    (_x_##LLU)

#endif  /* !defined(_MSC_VER) */

#if defined(_WIN32) || defined(_WIN64)
#   if _WIN64
#       define  BARRAGE_IS_PLATFORM_64BIT
#   else   /* _WIN64 */
#       define  BARRAGE_IS_PLATFORM_32BIT
#   endif  /* !_WIN64 */
#endif  /* defined(_WIN32) || defined(_WIN64) */

#if defined(__GNUC__)
#   if defined(__x86_64__) || defined(__ppc64__)
#       define  BARRAGE_IS_PLATFORM_64BIT
#   else   /* defined(__x86_64__) || defined(__ppc64__) */
#       define  BARRAGE_IS_PLATFORM_32BIT
#   endif  /* ! defined(__x86_64__) || defined(__ppc64__) */
#endif  /* defined(__GNUC__) */

namespace barrage {
    namespace misc {
        std::size_t MurmurHash::initialize() {
            return initialize( DEFAULT_SEED );
        }
        std::size_t MurmurHash::initialize( std::size_t seed ) {
            return seed;
        }

#if defined(BARRAGE_IS_PLATFORM_32BIT)

        std::size_t MurmurHash::update( ssize_t hash, std::size_t value ) {
            static const std::size_t C1 = 0xCC9E2D51;
            static const std::size_t C2 = 0x1B873593;

            std::size_t k1 = value;
            k1 *= C1;
            k1 = ROTL32( k1, 15 );
            k1 *= C2;

            hash ^= k1;
            hash = ROTL32( hash, 13 );

            return hash;
        }

        std::size_t MurmurHash::finish( std::size_t hash, std::size_t entry_count ) {
            hash ^= entry_count * 4;
            hash ^= hash >> 16;
            hash *= 0x85EBCA6B;
            hash ^= hash >> 13;
            hash *= 0xC2B2AE35;
            hash ^= hash >> 16;

            return hash;
        }

#else   /* defined(BARRAGE_IS_PLATFORM_32BIT) */

        std::size_t MurmurHash::update( ssize_t hash, std::size_t value ) {
            static const std::size_t C1 = BIG_CONSTANT( 0X87C37B91114253D5 );
            static const std::size_t C2 = BIG_CONSTANT( 0X4CF5AD432745937F );

            std::size_t k1 = value;
            k1 *= C1;
            k1 = ROTL64( k1, 31 );
            k1 *= C2;

            hash ^= k1;
            hash = ROTL64( hash, 27 );
            hash = hash * 5 + 0X52DCE729;

            return hash;
        }

        std::size_t MurmurHash::finish( std::size_t hash, std::size_t entry_count ) {
            hash ^= entry_count * 8;
            hash ^= hash >> 33;
            hash *= 0XFF51AFD7ED558CCD;
            hash ^= hash >> 33;
            hash *= 0XC4CEB9FE1A85EC53;
            hash ^= hash >> 33;

            return hash;
        }

#endif  /* !defined(BARRAGE_IS_PLATFORM_32BIT) */
    }
}