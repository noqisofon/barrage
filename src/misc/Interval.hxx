#pragma once

#include "barrage-common.hxx"

namespace barrage {
    namespace misc {
        /*!
         *
         */
        std::size_t numericToSymbol( ssize_t value ) {
            return static_cast<std::size_t>( value );
        }

        /*!
         *
         */
        ssize_t symbolToNumeric( std::size_t value ) {
            return static_cast<ssize_t>( value );
        }

        /*!
         *
         */
        class /* BARRAGE_CPP_PUBLIC */ Interval {
        public:
           /*!
            *
            */
            Interval();
            /*!
             *
             */
            explicit Interval( std::size_t a, std::size_t b );
            /*!
             *
             */
            Interval( ssize_t a, ssize_t b );

        public:
            /*!
             *
             */
            bool operator== ( const Interval &other ) const {
                return a == other.a && b == other.b;
            }

            /*!
             *
             */
            bool operator!= ( const Interval &other ) const {
                return *this == other;
            }

        public:
          /*!
           *
           */
            std::size_t length() const;
            /*!
             *
             */
            std::size_t hashCode() const;

            /*!
             *
             */
            bool startsBeforeDisjoint( const Interval &other ) const;

            /*!
             *
             */
            bool startsBeforeNonDisjoint( const Interval &other ) const;

            /*!
             *
             */
            bool startsAfter( const Interval &other ) const;

            /*!
             *
             */
            bool startsAfterDisjoint( const Interval &other ) const;

            /*!
             *
             */
            bool startsAfterNonDisjoint( const Interval &other ) const;

            /*!
             *
             */
            bool disjoint( const Interval &other ) const;

            /*!
             *
             */
            bool adjacent( const Interval &other ) const;

            /*!
             *
             */
            bool properlyContains( const Interval &other ) const;

            /*!
             *
             */
            Interval unionInterval( const Interval &other ) const;

            /*!
             *
             */
            Interval intersection( const Interval &other ) const;

            /*!
             *
             */
            std::string toString() const;

        public:
            static const Interval INVALID;

            ssize_t a;
            ssize_t b;
        };
    }
}