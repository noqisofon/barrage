#pragma once

#include "barrage-common.hxx"

namespace barrage {
    namespace dfa {
        /*!
         *
         */
        class Vocabulary {
        public:
            Vocabulary( Vocabulary const & ) = default;

            virtual ~Vocabulary();

        public:
            Vocabulary &operator = ( Vocabulary const & ) = default;

        public:
            /*!
             *
             */
            virtual std::string getDisplayName( std::size_t token_type ) const;
        };
    }
}